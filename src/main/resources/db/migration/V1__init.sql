CREATE TABLE IF NOT EXISTS users (
id_user INT AUTO_INCREMENT NOT NULL,
username VARCHAR (255) NOT NULL,
password VARCHAR (255) NOT NULL,
email VARCHAR (255) NOT NULL,
is_active BOOLEAN,
PRIMARY KEY (id_user)
);

CREATE TABLE IF NOT EXISTS category_cards (
id_category_card INT AUTO_INCREMENT NOT NULL,
description VARCHAR (255) NOT NULL,
PRIMARY KEY (id_category_card)
);


CREATE TABLE IF NOT EXISTS cards (
id_card INT AUTO_INCREMENT NOT NULL,
category_id INT,
name VARCHAR (255) NOT NULL,
attack INT NOT NULL,
defense INT NOT NULL,
description VARCHAR (255) NOT NULL,
PRIMARY KEY (id_card),
FOREIGN KEY (category_id) REFERENCES category_cards(id_category_card)
);

CREATE TABLE IF NOT EXISTS decks (
id_deck INT AUTO_INCREMENT NOT NULL,
user_id INT NOT NULL,
name VARCHAR (255) NOT NULL,
PRIMARY KEY (id_deck),
FOREIGN KEY (user_id) REFERENCES users(id_user)
);

CREATE TABLE IF NOT EXISTS cards_decks (
id_card INT NOT NULL,
id_deck INT NOT NULL,
PRIMARY KEY(id_card, id_deck),
FOREIGN KEY (id_card) REFERENCES cards(id_card),
FOREIGN KEY (id_deck) REFERENCES decks(id_deck)
);