INSERT INTO users (id_user, username, password, email, is_active) VALUES (1, 'urmano', 'urmano', 'test@gmail.com', true);

INSERT INTO cards (id_card, name, attack, defense, description) VALUES (1, 'test1', '4', 6, 'test');
INSERT INTO cards (id_card, name, attack, defense, description) VALUES (2, 'test2', '2', 8, 'test');
INSERT INTO cards (id_card, name, attack, defense, description) VALUES (3, 'test3', '5', 3, 'test');
INSERT INTO cards (id_card, name, attack, defense, description) VALUES (4, 'test4', '6', 5, 'test');
INSERT INTO cards (id_card, name, attack, defense, description) VALUES (5, 'test5', '1', 5, 'test');
INSERT INTO cards (id_card, name, attack, defense, description) VALUES (6, 'test6', '3', 2, 'test');
INSERT INTO cards (id_card, name, attack, defense, description) VALUES (7, 'test7', '5', 4, 'test');
INSERT INTO cards (id_card, name, attack, defense, description) VALUES (8, 'test8', '6', 8, 'test');
INSERT INTO cards (id_card, name, attack, defense, description) VALUES (9, 'test9', '9', 2, 'test');
INSERT INTO cards (id_card, name, attack, defense, description) VALUES (10, 'test10', '7', 5, 'test');