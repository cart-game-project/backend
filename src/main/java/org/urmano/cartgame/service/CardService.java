package org.urmano.cartgame.service;

import org.springframework.stereotype.Service;
import org.urmano.cartgame.dto.CardDto;
import org.urmano.cartgame.mapper.CardMapper;
import org.urmano.cartgame.model.Card;
import org.urmano.cartgame.repository.CardRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CardService {
    private final CardRepository cardRepository;
    private final CardMapper cardMapper;

    public CardService(CardRepository cardRepository, CardMapper cardMapper){
        this.cardRepository = cardRepository;
        this.cardMapper = cardMapper;
    }

    public List<CardDto> get(){
        return cardRepository.findAll().stream().map(this::map).collect(Collectors.toList());
    }

    private CardDto map(Card in){
        CardDto out = cardMapper.map(in);
        return out;
    }
}
