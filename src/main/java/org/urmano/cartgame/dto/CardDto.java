package org.urmano.cartgame.dto;

import lombok.*;

@Builder
@Setter
@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class CardDto {
    private Integer id;
    private String name;
    private Integer attack;
    private Integer defense;
    private String description;
}
