package org.urmano.cartgame.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.urmano.cartgame.dto.CardDto;
import org.urmano.cartgame.service.CardService;

import java.util.List;

@RestController
@RequestMapping("/cards")
public class CardController {
    private final CardService cardService;

    public CardController(CardService cardService){
        this.cardService = cardService;
    }

    @GetMapping
    public ResponseEntity<List<CardDto>> get() {
        return new ResponseEntity<>(cardService.get(), HttpStatus.OK);
    }
}
