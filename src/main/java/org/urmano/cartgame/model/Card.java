package org.urmano.cartgame.model;

import lombok.*;
import org.urmano.cartgame.dto.CardDto;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Table
@Entity(name= "cards")
public class Card {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_card")
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "attack")
    private Integer attack;
    @Column(name = "defense")
    private Integer defense;
    @Column(name = "description")
    private String description;

    @ManyToMany
    @JoinTable(
            name="cards_decks",
            joinColumns = @JoinColumn(name="id_card"),
            inverseJoinColumns = @JoinColumn(name="id_deck")
    )
    private List<Deck> deckList;

//    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "card")
//    private List<Deck> deckList = new ArrayList<>();

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "category_id")
    private CategoryCard categoryCard;
}
