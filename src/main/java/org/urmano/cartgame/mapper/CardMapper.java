package org.urmano.cartgame.mapper;

import org.mapstruct.Mapper;
import org.urmano.cartgame.dto.CardDto;
import org.urmano.cartgame.model.Card;

import java.util.List;

@Mapper
public interface CardMapper {
    CardDto map(Card in);
    List<CardDto> map(List<Card> in);
}