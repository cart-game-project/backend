package org.urmano.cartgame.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.urmano.cartgame.model.Deck;

@Repository
public interface DeckRepository extends JpaRepository<Deck, Integer> {
}
