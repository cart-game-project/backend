package org.urmano.cartgame.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.urmano.cartgame.model.Card;

@Repository
public interface CardRepository extends JpaRepository<Card, Integer> {
}
